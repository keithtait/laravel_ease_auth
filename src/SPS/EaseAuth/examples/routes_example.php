<?php

Route::group([
    'before'  => 'ease',
], function()
    {
        Route::any('/', [
            'as'    => 'login',
            'uses'  => 'UsersController@loginAction'
        ]);
    }
);

Route::group([
    'before'  => 'auth',
], function()
    {
        Route::any('logout', [
            'as'    => 'logout',
            'uses'  => 'UsersController@logoutAction'
        ]);
        
        Route::any('dashboard', [
            'as'    => 'dashboard',
            'uses'  => 'UsersController@viewAction'
        ]);
    }
);
