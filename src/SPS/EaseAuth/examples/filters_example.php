<?php

Route::filter('ease', function()
{
	if ( ! Auth::ease()) return Redirect::away(Config::get('ease.login'));
});
