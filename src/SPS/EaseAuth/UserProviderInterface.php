<?php namespace SPS\EaseAuth;

interface UserProviderInterface {

	/**
	 * Retrieve a user by their unique identifier.
	 *
	 * @param  mixed  $identifier
	 * @return \SPS\EaseAuth\UserInterface|null
	 */
	public function retrieveById($identifier);

	/**
	 * Retrieve a user by the given credentials.
	 *
	 * @param  array  $credentials
	 * @return \SPS\EaseAuth\UserInterface|null
	 */
	public function retrieveByCredentials(array $credentials);

}