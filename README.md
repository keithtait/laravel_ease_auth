#laravel_ease_auth

Ease authentication for the Laravel PHP framework.

This is designed to be a drop-in replacement for the built-in Laravel Auth module.

The current version is for use with laravel 4.1.*

**A review is needed in respect of changes to the standard User model in Laravel 4.2**

##Installation

Add the following to the require section of your project's composer.json:  
```
"keithtait/laravel_ease_auth": "dev-master"
```

Update the project's dependencies:  
```
$ composer update
```

Add the provider to app/config/app.php, replace:
```php
'Illuminate\Auth\AuthServiceProvider'
```
with
```php
'SPS\EaseAuth\AuthServiceProvider'
```

Add the alias to app/config/app.php, replace
```php
'Illuminate\Support\Facades\Auth'
```
with
```php
SPS\EaseAuth\AuthFacade'
```

###Configuration

The EASE Auth module requires very simple configuration:

1. Copy the 'ease' settings in the example file _config_example.php_ to _app/config/auth.php_

2. Add the 'ease' filter in the example file _filters_example.php_ to _app/filters.php_

##Usage

Using the EASE Auth module is almost exactly the same as the usual Illuminate Auth that ships with Laravel. The major difference is that it does not process user passwords.

It accepts that once a user has been authenticated by EASE all it need do is check that the current user is authorised to access resouces within the application.

This also means that there is no need to create either login or password reminder forms.

If you're converting an existing app with Illuminate Auth to EASE Auth the passwords stored for each user will simply be ignored.

Example of routes and controller usage can be found in the included files _routes_example.php_ and _controller_example.php_